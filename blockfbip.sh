#/bin/bash

#You should add a toblockip.dat file in the same directory of the script
#listing additional ip adresses you wish to block

bra="fbblock"
brb="fbblock2"
dir=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )

if [ "$#" -gt "0" ]
then
	iptables -t filter -D INPUT -j "$bra"
	iptables -t filter -D OUTPUT -j "$bra"
	iptables -t filter -D FORWARD -j "$bra"
	iptables -F "$bra"
	iptables -X "$bra"
	exit 0
fi

#check if chain exists
fbip=$(whois -h 'whois.radb.net' '!gAS32934' | grep '/')
fbipc=$(echo "$fbip" | wc -l)
ec=$(sudo iptables -L -n | grep 'Chain fbblock ' | wc -l)
echo "Chains with that name: $ec"
echo "Number of subnets: $fbipc"

#Only replace if the script was able to download a new list of ip adresses
if [ "$fbipc" -gt 0 ]
then

	iptables -N "$brb"
	iptables -t filter -A INPUT -j "$brb"
	iptables -t filter -A OUTPUT -j "$brb"
	iptables -t filter -A FORWARD -j "$brb"

	for ip in $fbip
	do
		echo -n "Blocking $ip... "
		iptables -A "$brb" -p all -s "$ip" -j DROP
		iptables -A "$brb" -p all -d "$ip" -j DROP
		echo "[done]"
	done

	for ip in `host -t a fbstatic-a.akamaihd.net | grep -P -o '\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}'`
	do
		echo -n "Blocking $ip... "
		iptables -A "$brb" -p all -s "$ip" -j DROP
		iptables -A "$brb" -p all -d "$ip" -j DROP
		echo "[done]"
	done
	
	while read ip
	do
		echo -n "Blocking $ip... "
		iptables -A "$brb" -p all -s "$ip" -j DROP
		iptables -A "$brb" -p all -d "$ip" -j DROP
		echo "[done]"
	done < "$dir/toblockip.dat"
	
	
	#remove the old chain and rename the first one
	if [ "$ec" -gt 0 ]
	then
		iptables -t filter -D INPUT -j "$bra"
		iptables -t filter -D OUTPUT -j "$bra"
		iptables -t filter -D FORWARD -j "$bra"
		iptables -F "$bra"
		iptables -X "$bra"
	fi
	iptables -E "$brb" "$bra"
	
fi
