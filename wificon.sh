#!/bin/bash
sudo rfkill block all >/dev/null 2>/dev/null
sleep 5
sudo rfkill unblock all >/dev/null 2>/dev/null
sleep 5
sts=0

ct="$CONN/eduroam.conf"

if [ "$#" -gt "0" ]
then
    ct="$1"
fi

if [ ! -f "$ct" ]
then
    ct="$CONN/$ct.conf"
fi

while [ "$sts" -eq "0" ]
do
    sudo killall wpa_supplicant >/dev/null 2>/dev/null
    sts="$?"
    sleep 1
done
sudo wpa_supplicant -Dnl80211 -iwlan0 -c"$ct" -B
sleep 1
sts=0
while [ "$sts" -eq "0" ]
do
    sudo killall wpa_supplicant >/dev/null 2>/dev/null
    sts="$?"
    sleep 1
done
sleep 1
sudo wpa_supplicant -Dnl80211 -iwlan0 -c"$ct" -B
sleep 1
sudo dhclient wlan0
sudo tsocks -on
sudo bash "$SCRP/blockfbip.sh" >/dev/null 2>/dev/null
