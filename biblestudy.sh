#!/bin/bash

fle="biblestudy"
sep=":"

if [ ! -f "$ORGN/$fle" ]
then
    echo 1>&2 "Make sure the file \"$fle\" is placed on the organizer folder..."
    exit 1
fi

chp=$(head -n 1 "$ORGN/$fle")
chn=$(bible -f "$chp-Rev22:21" | grep -v "^$chp" -m 1 | grep -o -P '^[^:]*')

echo "$chn" > "$ORGN/$fle"

bible "$chp-$chn:0" | espeak &
pid="$!"

bible "$chp-$chn:0" | less -r

kill "$pid"
