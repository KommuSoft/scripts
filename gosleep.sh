#!/bin/bash

sudo rfkill block all 2>/dev/null

#Daily hygiene questions
st=0
read -p "Tanden gepoetst [yN]? " -n 1 -r s; echo;      if [[ ! $s =~ ^[nN]$ ]]; then st=$((st+1)); fi
read -p "Gedouched [yN]? " -n 1 -r s; echo;            if [[ ! $s =~ ^[nN]$ ]]; then st=$((st+2)); fi
read -p "Pyama aan [yN]? " -n 1 -r s; echo;            if [[ ! $s =~ ^[nN]$ ]]; then st=$((st+4)); fi
read -p "Voldoende ventilatie [yN]? " -n 1 -r s; echo; if [[ ! $s =~ ^[nN]$ ]]; then st=$((st+8)); fi
read -p "Gsm uitgezet [yN]? " -n 1 -r s; echo;         if [[ ! $s =~ ^[nN]$ ]]; then st=$((st+16)); fi
read -p "Hygiene doel gehaald [yN]? " -n 1 -r s; echo; if [[ ! $s =~ ^[nN]$ ]]; then st=$((st+32)); fi

#Day context
hydt='-10 hours'

hyfl=$(date '+%Y%m' -d "$hydt")
hyfl="$DARY/$hyfl-hy.une"
hyky=$(date '+%d' -d "$hydt")
hysp=":"

echo -n "$hyky$hysp$st" >> "$hyfl"
bash "$DARY/encrhy.sh"

#Diary check
dydt='-10 hours'
dyfl=$(date '+%Y%m%d' -d "$dydt")
dyfla="$dyfl.md"

if [ ! -f  "$DARY/$dyfla" ]
then
    echo "Je hebt het dagboek voor vandaag nog niet ingevuld..."
    read -p "Wil je het dagboek nu invullen [Yn]? " -n 1 -r
    echo
    if [[ ! $REPLY =~ ^[nN]$ ]]
    then
        bash "$DARY/diary.sh"
    fi
    clear
fi

#Donation check
bash "$SCRP/donation.sh"

#Evening prayers
read -p "Wil je een hoofdstuk uit de bijbel horen? [Yn]? " -n 1 -r
echo
if [[ ! $REPLY =~ ^[nN]$ ]]
then
    #clear
    #pry=$(find "/home/kommusoft/writing/prayers/" -type f | shuf -n1)
    #less -r "$pry"
    bash "$SCRP/biblestudy.sh"
    clear
fi

#Bedtime story
read -p "Wil je een slaapverhaaltje horen [Yn]? " -n 1 -r; echo
if [[ ! $REPLY =~ ^[nN]$ ]]
then
    clear
    bash "$SCRP/bedtimestory.sh"
    clear
fi

#Start sleeping
cmus -C 'quit'
setterm -blank 1 -powerdown 1
bash -c "sleep 3; cmus-remote -C 'filter filename=\"*/Meditative/*\"'; cmus-remote -p -v 20" &
ta=$(date +%s)
cmus

#End sleeping, calculate time and rating
clear
tb=$(date +%s)
tc=$(($tb-$ta))
read -p "Uitgeslapen [0-9]? " -n 1 -r rat; echo
echo "$hysp$tc$hysp$rat" >> "$hyfl"
clear
