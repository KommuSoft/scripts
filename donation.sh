#!/bin/bash

dt=1209600 #Minimum number of seconds between two donations
fl="$ORGN/donation"

last=$(head -n 1 "$fl")
tday=$(date '+%D' -d '-10 hours')
curr=$(date '+%s' -d "0:00 $tday")

dffr=$(($curr-$last))

if [ "$dffr" -gt "$dt" ]
then
    read -p "Heb je bloed gegeven [yaN]?" -n 1 -r s; echo;
    if [[ $s =~ ^[yY]$ ]]
    then
        echo "$curr" > "$fl"
    elif [[ $s =~ ^[aA]$ ]]
    then
        read -p "Op welke dag heb je bloed gegeven (date format)?" -r s; echo;
        date '%s' -d "0:00 $s" > "$fl" #TODO: recheck?
    else
        exit 1
    fi
fi
exit 0
