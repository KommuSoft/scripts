#!/bin/bash

for e in m4a
do
    for f in *.$e
    do
        bn=$(basename "$f" ".$e")
        avconv -i "$f" "$bn.mp3"
        id3v2 -t "$f" "$bn.mp3"
        srm "$f"
    done
done
