#!/bin/bash

fle="bedtimestory"
sep=":"

if [ ! -f "$ORGN/$fle" ]
then
    echo 1>&2 "Make sure the file \"$fle\" is placed on the organizer folder..."
    exit 1
fi

pdf=$(head -n 1 "$ORGN/$fle")
chp=$(echo "$pdf" | cut -d "$sep" -f 2)
pdf=$(echo "$pdf" | cut -d "$sep" -f 1)

chp=$(($chp+1))

echo "$pdf$sep$chp" > "$ORGN/$fle" #Mark for next time
bash "$SCRP/readchapter.sh" "$pdf" "$chp"
