#!/bin/bash
rfkill block all
if [ "$#" -lt "1" ]
then
    echo 1>&2 'You must provide at least the name of the file to write to'
    exit 1
fi
echo 1>&2 "Writing to \"$1\"..."
sleep 1

tmpf=$(tempfile -d '/tmp/')

vim -n "$tmpf"
clear
ext=0
if [ -a "$tmpf" ]
then
    gpg -er Sheldon -o "$1" "$tmpf"
    srm "$tmpf"
    free && sync && free
    chmod 400 "$1"
else
    ext=1
fi
free && sync && free
clear
free && sync && free
clear
exit "$ext"
