#!/bin/bash

#Parameters
chr='^\s*chapter'

#Parsing arguments
fle="$1"

#Fetch pdf text
txt=$(pdftotext "$1" -)

if [ "$#" -gt "1" ]
then
    chn="$2"
    chm=$(($chn+1))

    #todo: processing text (removing page numbers, etc.)

    #Find boundaries
    bdr=$(echo "$txt" | grep -o -i -n -m "$chm" -P "$chr" | cut -d ':' -f 1 | tail -n "+$chn")

    bda=$(echo "$bdr" | head -n 1)
    bdb=$(echo "$bdr" | tail -n +2)
    bdb=$(($bdb-1))

    #Extract text
    txt=$(echo "$txt" | head -n "$bdb" | tail -n "+$bda")
    echo "$txt" | espeak &
    pid="$!"
    echo "$txt" | less
    kill "$pid"
else
    echo "$txt" | grep -i -P "$chr" | wc -l
fi
