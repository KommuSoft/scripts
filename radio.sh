#!/bin/bash
st="?"    #The name of the radio station
lst="0"    #Boolean indicating whether a list of stations must be emitted
tr="0"    #Boolean indicating whether the stream should be received through tor
ch="1000" #The number of milliseconds vlc will cache
rdf="$CONN/radios.map"

while [ $# -gt 0 ]
do
    case $1 in
        "-L")
            lst="1"
            ;;
        "-t")
            tr="1"
            ;;
        *)
            st="$1"
            ;;
    esac
    shift
done
if [ "$lst" -eq "1" ]
then
    cut -d':' -f 1 "$rdf" | sort
else
    url=$(grep -m 1 -h -i "^$st:" "$rdf" | cut -d':' -f 2-)
    if [ ! -z "$url" ]
    then
        if [ $tr -eq 1 ]
        then
            torsocks vlc -q --udp-caching=$ch --tcp-caching=$ch --realrtsp-caching=$ch "$url" 2>/dev/null
        else
            vlc -q --udp-caching=$ch --tcp-caching=$ch --realrtsp-caching=$ch "$url" 2>/dev/null
        fi
    else
        echo "Could not find radio station \"$st\""
    fi
fi
