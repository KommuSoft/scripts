#!/usr/bin/env python

from __future__ import print_function
import httplib2
import os

from apiclient import discovery
import oauth2client
from oauth2client import client
from oauth2client import tools

import datetime
import dateutil
from dateutil.parser import parse
import sys
import json
import itertools

#try:
#    import argparse
#    flags = argparse.ArgumentParser(parents=[tools.argparser]).parse_args()
#except ImportError:

fetch=False

if len(sys.argv) > 1 :
    cmd=sys.argv[1]
    if cmd == 'fetch' :
        fetch = True

flags = None

SCOPES = 'https://www.googleapis.com/auth/calendar.readonly'
CLIENT_SECRET_FILE = 'paulas_secret.json'
APPLICATION_NAME = 'life'
nev=10

datfile = os.path.join(os.environ['ORGN'],'calendar.json')
tokfile = os.path.join(os.environ['CONN'],'gcal-token.json')
muar=['1;34','34','36','1;35','32','1;33','33','31','1;31','1;35','37','1;31']

def get_credentials():
    store = oauth2client.file.Storage(tokfile)
    credentials = store.get()
    if not credentials or credentials.invalid:
        flow = client.flow_from_clientsecrets(CLIENT_SECRET_FILE, SCOPES)
        flow.user_agent = APPLICATION_NAME
        if flags:
            credentials = tools.run_flow(flow, store, flags)
        else: 
            credentials = tools.run(flow, store)
        print('Storing credentials to ' + credential_path)
    return credentials

def main():
    if fetch :
        credentials = get_credentials()
        http = credentials.authorize(httplib2.Http())
        service = discovery.build('calendar', 'v3', http=http)

        now = datetime.datetime.utcnow().isoformat() + 'Z' # 'Z' indicates UTC time
        eventsResult = service.events().list(calendarId='primary',timeMin=now,maxResults=128,singleEvents=True,orderBy='startTime').execute()
        f = open(datfile,'w')
        f.write(json.dumps(eventsResult))

    else :
        with open(datfile) as f:
            eventsResult = json.load(f)

    events = eventsResult.get('items', [])

    #"""
    if not events:
        print('No upcoming events found.')
    dya = parse(datetime.datetime.utcnow().isoformat() + 'Z')
    dyb = dya+datetime.timedelta(1,0)
    for event in events[:nev]:
        pre = "\x1b["
        start = event['start'].get('dateTime',event['start'].get('date'))
        end = event['end'].get('dateTime',event['end'].get('date'))
        tmi = parse(start)
        tmj = parse(end)

        if tmj < dya :
            continue
        if tmi > dyb :
            break

        start = tmi.strftime('%d-%b %H:%M')
        clr = muar[0]
        if 'colorId' in event :
		    clr=muar[int(event['colorId'])%len(muar)]
        print(pre+clr+"m",start, event['summary'],"\x1b[0m")
    #"""


if __name__ == '__main__':
    main()
